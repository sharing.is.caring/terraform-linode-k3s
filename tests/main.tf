
terraform {
  required_providers {
    random = {
      source = "hashicorp/random"
      version = "3.4.3"
    }
  }
  backend "http" {
  }
}

provider "random" {

}

locals {
  ssh_key_pub = var.ssh_key_pub == "" ? "${var.ssh_key}.pub" : var.ssh_key_pub

}

resource "random_password" "archrootpass" {
  length           = 16
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

resource "random_password" "kubernetespass" {
  length           = 16
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

module "linode_k3s" {
  source                   = "gitlab.com/privateprojectz/terraform-linode-k3s/linode"
  version                  = "0.1.12"
  #version                  = "VERSION"
  linodeapikey             = var.linode_api_key
  unique_number            = 27
  archrootpass             = random_password.archrootpass.result
  ssh_key_pub              = local.ssh_key_pub
  ssh_key                  = var.ssh_key
  linode_username          = var.linode_username
  kubernetes_user_name     = "kuber"
  kubernetes_user_password = random_password.kubernetespass.result
  worker_count             = 1

}
