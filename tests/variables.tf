variable "linode_api_key" {
    type = string
    sensitive = true
}

variable "ssh_key" {
    type = string
    sensitive = true
}

variable "ssh_key_pub" {
    type = string
    default = ""
}


variable "linode_username" {
    type = string
}

variable "archrootpass" {
    type = string
    sensitive = true
    default = ""
}

variable "kubernetes_user_password" {
    type = string
    sensitive = true
    default = ""
}

