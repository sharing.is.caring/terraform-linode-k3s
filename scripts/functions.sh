#!/usr/bin/env bash


shopt -s expand_aliases
alias convert_report="jq -r '([.resource_changes[]?.change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'"


 function tf_init_gitlab () {
  terraform init \
    -plugin-dir=/plugins \
    -backend-config=address=${TF_ADDRESS} \
    -backend-config=lock_address=${TF_ADDRESS}/lock \
    -backend-config=unlock_address=${TF_ADDRESS}/lock \
    -backend-config=username=${TF_USERNAME} \
    -backend-config=password=${CI_DEPLOY_TOKEN} \
    -backend-config=lock_method=POST \
    -backend-config=unlock_method=DELETE \
    -backend-config=retry_wait_min=5
 }

  
 function tf_init_gitlab_download_plugins () {
  terraform init \
    -backend-config=address=${TF_ADDRESS} \
    -backend-config=lock_address=${TF_ADDRESS}/lock \
    -backend-config=unlock_address=${TF_ADDRESS}/lock \
    -backend-config=username=${TF_USERNAME} \
    -backend-config=password=${CI_DEPLOY_TOKEN} \
    -backend-config=lock_method=POST \
    -backend-config=unlock_method=DELETE \
    -backend-config=retry_wait_min=5
 }

 function tf_init () {
  terraform init -plugin-dir=/plugins 
 }

  
 function tf_init_download_plugins () {
  terraform init 
 }

function _tagAndPush () {
        NEW_TAG="v$major.$minor.$patch"
        echo "Tagging release $NEW_TAG."
        git tag -a $NEW_TAG -m "release version ${NEW_TAG}"
        git push origin --tags
        echo "Latest tag is now $(git describe --tags)"
}

function _tagFromCommit () {

        IFS=. read major minor patch <<<"${CURRENT_RELEASE##*v}"
        if [[ "${CI_COMMIT_MESSAGE}" == *'[tag_minor]'* ]]; then
            echo "Minor release version will be incremented."
            ((minor++))
            _tagAndPush
        elif [[ "${CI_COMMIT_MESSAGE}" == *'[tag_major]'* ]]; then
            echo "Major release version will be incremented."
            ((major++))
            _tagAndPush
        elif [[ "${CI_COMMIT_MESSAGE}" == *'[tag_patch]'* ]]; then
            echo "Patch release version, $patch,  will be incremented."
            ((patch++))
            _tagAndPush
        else
            echo "do nothing"
        fi 
}


function tagRelease () {

    if [[ $CI_COMMIT_BRANCH = "master" ]]; then

        # Gitlab-ci appends to the version so get rid of that
        git clone $CI_REPOSITORY_URL
        cd ${CI_PROJECT_NAME}
        git checkout ${CI_COMMIT_REF_NAME}
        CURRENT_RELEASE=$(git describe --tags | cut -d "-" -f 1) #$(git rev-list --tags --max-count=1))
        result=$?

        if [[ $result = 128 ]]; then 

            echo "no tag found"
            CURRENT_RELEASE="v0.0.0"
        fi
        _tagFromCommit
        cd ..
    else
        exit 0
    fi
}

########################################################
############### Git Helper #############################
########################################################

function git_helper() {
    git config --global user.name "${GITLAB_USER_NAME}"
    git config --global user.email "${GITLAB_USER_EMAIL}"

    #rm -rf ~/.git-credentials
    #git config --global credential.helper store
    #git config --global credential.${CI_SERVER_URL}.username gitlab-deploy-token
    #git config --global user.username "gitlab-deploy-token" # GITLAB_USER_LOGIN
    #git config --global  user.email "gitlab-deploy-token@${CI_SERVER_HOST}" # $GITLAB_USER_EMAIL
    #echo "https://gitlab-deploy-token:${CI_DEPLOY_TOKEN}@${CI_SERVER_HOST}" >> ~/.git-credentials
    #git config --global credential.helper 'store --file ~/.git-credentials'
    #chmod +x ~/.git-credentials
    #git config --list
}



########################################################
############### Check TF Format ########################
########################################################

function tf_check_fmt() {
    set +e
    terraform fmt -check -recursive
    result=$?
    echo "Last exist code: $result"
    echo "ran from: $(pwd)"
    set -e
    if [ $result -ne 0 ]; then
        echo "There are formatting errors. Please review the changes below."
        echo "If the changes are acceptable, run "terraform fmt -recursive" on the root project and update the merge request."
        terraform fmt -diff -recursive
        exit 1
    fi
}

########################################################
############### Auto TF Format #########################
########################################################


function tf_autofmt() {

    git_helper
    git clone ${CI_REPOSITORY_URL}
    cd ${CI_PROJECT_NAME}
    git checkout ${CI_COMMIT_REF_NAME}
    terraform fmt -recursive
    git commit -am "[ci skip] ${CI_COMMIT_REF_NAME} Gitlab CI auto-format tf files."    
    git push "https://${GITLAB_USER_LOGIN}:${CI_DEPLOY_TOKEN}@${CI_REPOSITORY_URL#*@}" ${CI_COMMIT_REF_NAME}
}

function auto_terradoc() {
    set +ex
    git_helper
    git clone ${CI_REPOSITORY_URL}
    cd ${CI_PROJECT_NAME}
    git checkout ${CI_COMMIT_REF_NAME}
    terraform-docs markdown table --output-file README.md --output-mode inject .
    git commit -am "[ci skip] ${CI_COMMIT_REF_NAME} Gitlab CI update terraform docs $(date)."

    git push -o ci-skip "https://${GITLAB_USER_LOGIN}:${CI_DEPLOY_TOKEN}@${CI_REPOSITORY_URL#*@}" ${CI_COMMIT_REF_NAME}
    set -e
}

########################################################
############### Remove Lock gitlab #####################
########################################################


function remove_lock_gitlab() {

    curl -X GET -H "authorization: Bearer ${CI_DEPLOY_TOKEN}" "${TF_ADDRESS}/lock"
    result=$?


    if [ $result -eq 0 ]; then

        for lock in $locks; do
            printf "=========================\n\nRemoving lock %s\n=========================\n\n" $lock
            curl -X DELETE -H "authorization: Bearer ${CI_DEPLOY_TOKEN}" ${TF_ADDRESS}/lock
            result=$?
            if [ $result -ne 0 ]; then
                echo "There were errors trying to remove the locks."
                echo "Please try again or manually remove in gitlab."
                exit 1
            fi
        done
    fi
}


