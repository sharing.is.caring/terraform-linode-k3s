terraform {
  required_providers {
    linode = {
      source = "linode/linode"
    }
  }
  required_version = ">= 1.2.6"
}
